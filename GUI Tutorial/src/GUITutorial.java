import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GUITutorial extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUITutorial frame = new GUITutorial();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUITutorial() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Hallo Welt!");
		lblNewLabel.setBounds(84, 28, 193, 14);
		contentPane.add(lblNewLabel);
		
		JButton btnNewButton = new JButton("RED");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonRed_clicked();
			}
		});
		btnNewButton.setBounds(31, 128, 89, 23);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("BLUE");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonBlue_clicked();
			}
		});
		btnNewButton_1.setBounds(220, 128, 89, 23);
		contentPane.add(btnNewButton_1);
	}

	protected void buttonBlue_clicked() {
		this.contentPane.setBackground(Color.BLUE);
		
	}

	protected void buttonRed_clicked() {
		this.contentPane.setBackground(Color.RED);
		
	}
}
